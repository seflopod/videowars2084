﻿using UnityEngine;
using System.Collections.Generic;

public class GUIBehaviour : MonoBehaviour
{
	public GUISkin skin;

	private string[] _styleNames = new string[2]{"player 1", "player 2"};
	private bool _dispStart = true;
	private SimpleTimer _creditsTimer = new SimpleTimer(500.0);

	private void Start()
	{
		_creditsTimer.Start();
	}

	private void OnGUI()
	{
		checkSkin();
		GUILayout.BeginArea(new Rect(0,0,Screen.width, gameObject.camera.rect.height*Screen.height));
		GUILayout.BeginHorizontal();

		switch(GameManager.Instance.State)
		{
		case GameState.PlayGame:
			PlayerBehaviour[] p = GameManager.Instance.Players;

			if(p[0].State.inGame && !p[0].IsAI)
				drawScores(0);
			else
				drawStart(0);

			GUILayout.FlexibleSpace();

			if(p[1].State.inGame && !p[1].IsAI)
				drawScores(1);
			else
				drawStart(1);

			break;
		default:
			drawStart(0);
			GUILayout.FlexibleSpace();
			drawStart(1);
			break;
		}

		GUILayout.EndHorizontal();
		GUILayout.EndArea();
	}

	private void checkSkin()
	{
		if(skin != null && GUI.skin != skin)
			GUI.skin = skin;
	}

	private void drawScores(int idx)
	{
		GUIStyle style = skin.GetStyle(_styleNames[idx]);

		GUILayout.BeginVertical();
		GUILayout.FlexibleSpace();

		GUILayout.Label(_styleNames[idx], style);
		GUILayout.FlexibleSpace();

		string score = Mathf.Max(0,GameManager.Instance.Scores[idx].Total).ToString("D8");
		GUILayout.Label(score, style);
		GUILayout.FlexibleSpace();

		GUILayout.Label("Kills", style);
		GUILayout.Label(GameManager.Instance.Scores[idx].kills.ToString(), style);
		GUILayout.FlexibleSpace();

		GUILayout.EndVertical();
	}

	private void drawStart(int idx)
	{
		GUIStyle style = skin.GetStyle(_styleNames[idx]);
		
		GUILayout.BeginVertical();
		GUILayout.FlexibleSpace();
		
		GUILayout.Label(_styleNames[idx], style);
		GUILayout.FlexibleSpace();

		string lbl;
		if(_dispStart) lbl = "Press Start";
		else lbl = "------------";
		GUILayout.Label(lbl, style);
		GUILayout.FlexibleSpace();

		GUILayout.EndVertical();

		if(_creditsTimer.IsExpired)
		{
			_dispStart = !_dispStart;
			_creditsTimer.Stop();
			_creditsTimer.Start();
		}
	}
}

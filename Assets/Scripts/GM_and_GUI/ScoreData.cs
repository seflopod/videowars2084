﻿using UnityEngine;

public class ScoreData
{
	public int kills = 0;
	public int deaths = 0;
	public int bulletsFired = 0;
	public int bulletsHit = 0;
	
	public void Reset()
	{
		kills = 0;
		deaths = 0;
		bulletsFired = 0;
		bulletsHit = 0;
	}

	public int Total
	{
		get
		{
			return Mathf.Max(0, 1000*kills + 100*bulletsHit - 500*deaths);
		}
	}
}
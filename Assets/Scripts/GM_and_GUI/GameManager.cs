using UnityEngine;
using System.Collections.Generic;

public enum GameState
{
	TitleScreen,
	DemoScreen,
	CreditsScreen,
	SceneTransition,
	ShowScores,
	PlayGame
};

public class GameManager : Singleton<GameManager>
{
	#region fields
	public string[] levelNames;
	public Tileset tiles;
	public GameObject bulletPrefab;
	public GameObject[] playerPrefabs;
	public int creditsToStart = 1;
	public float respawnPenalty = 1f;
	public GameObject deathSoundPrefab;
	public AudioClip death;
	public AudioClip damage;
	public AudioClip fire;
	public int killsToEnd = 5; //start low, should be higher though

	private Camera _mainCam;
	private Vector3[] _spawnPoints;
	private int _spwnIdx;
	private Keymap[] _keymaps;
	private PlayerBehaviour[] _players;
	private Queue<PlayerBehaviour> _toRespawn;
	private BulletManager[] _bullets;
	private int _bulletIdx;
	private Vector2 _dim;
	private GameState _state;
	private ScoreData[] _scores;
	private bool[] _started;
	private int _curLevelIdx;
	private SimpleTimer _contTimer;
	private bool _keepScores;
	#endregion

	#region monobehaviour
	protected override void Awake()
	{
		base.Awake();

		//need to stay around through multiple scene transitions
		DontDestroyOnLoad(gameObject);
	}

	/// <summary>
	/// Start this instance.
	/// </summary>
	private void Start()
	{
		//stupid way of dealing with sounds
		PlayerBehaviour.DeathSound = death;
		PlayerBehaviour.DamageSound = damage;
		PlayerBehaviour.FireSound = fire;

		//setup keymaps
		_keymaps = new Keymap[2] { new Keymap(1), new Keymap(2) };
		_players = new PlayerBehaviour[playerPrefabs.Length];

		//score tracking
		_scores = new ScoreData[playerPrefabs.Length];
		for(int i=0;i<playerPrefabs.Length;++i) _scores[i] = new ScoreData();

		//get cameras for later use
		_mainCam = Camera.main;

		//know whether or not a player is actually in game
		_started = new bool[playerPrefabs.Length];
		for(int i=0;i<playerPrefabs.Length;++i) _started[i] = false;

		//variables pertaining to the end of the level
		_contTimer = new SimpleTimer(10000.0);
		_keepScores = false;

		//tracking where we are in the game regarding scene and level
		_curLevelIdx = 0; //to know which level to load

		_state = GameState.TitleScreen;
	}

	private void Update()
	{
		float dt = Time.deltaTime;

		if(!Application.isLoadingLevel)
		{
			processPlayerInput(dt);
			/*
			switch(_state)
			{
			case GameState.PlayGame:
				processPlayerInput(dt);
				break;
			default:
				processPlayerInput(dt);
				break;
			}
			*/
		}
	}

	private void LateUpdate()
	{
		//respawn as needed
		if(_state == GameState.PlayGame && _toRespawn != null) respawnPlayers();
	}

	private void OnLevelWasLoaded(int lvl)
	{
		if(lvl == 0)
		{
			_state = GameState.TitleScreen;
			Start(); //seems like a good way to restart
		}
		else if(lvl == 1) //just go with it
		{
			populateNewLevel();
		}
		else if(lvl == 2) //scores
		{
			_contTimer.Start();

			//when I actually get something showing for a scores screen, this should be
			//put at the end of whatever function sets that up.
			_state = GameState.ShowScores;
		}
	}
	#endregion

	#region input_handling
	/// <summary>
	/// Processes the player input.
	/// </summary>
	/// <param name="dt">The change in time from the last frame</param>
	private void processPlayerInput(float dt)
	{
		if(Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();

		for(int i=0;i<2;++i)
		{
			switch(_state)
			{
			case GameState.PlayGame:
				if(_started[i])
				{
					if(!_players[i].IsAI && !_players[i].State.dead)
						playerMovementInput(i, dt);
				}
				else
				{
					playerCoinsInput(i);
				}
				break;
			case GameState.ShowScores:
				playerScoresInput(i);
				break;
			default:
				playerCoinsInput(i);
				break;
			}
		}
	}

	/// <summary>
	/// Players the movement input.
	/// </summary>
	/// <param name="idx">Index.</param>
	/// <param name="dt">Dt.</param>
	private void playerMovementInput(int idx, float dt)
	{
		if(Input.GetKey(_keymaps[idx].down)) _players[idx].Stop();
		
		Vector2 moveDir = Vector2.zero;
		if(Input.GetKey(_keymaps[idx].up)) moveDir.y += 1;
		if(Input.GetKey(_keymaps[idx].left)) moveDir.x -= 1;
		if(Input.GetKey(_keymaps[idx].right)) moveDir.x += 1;
		if(moveDir != Vector2.zero)
			_players[idx].Move(moveDir);
		
		if(Input.GetKey(_keymaps[idx].button7)) _players[idx].Thrust(dt);
		if(Input.GetKeyUp(_keymaps[idx].button7)) _players[idx].EndThrust();
		if(Input.GetKeyDown(_keymaps[idx].button4)) _players[idx].Fire(true);
		else if(Input.GetKeyDown(_keymaps[idx].button5)) _players[idx].Fire(false);
	}

	/// <summary>
	/// Players the coins input.
	/// </summary>
	/// <param name="idx">Index.</param>
	private void playerCoinsInput(int idx)
	{
		if(Input.GetKeyDown(_keymaps[idx].start) && !_started[idx])
		{
			_started[idx] = true;
			if(_state == GameState.PlayGame)
			{
				spawnPlayer(idx, _mainCam.GetComponent<CameraBehaviour>());
				if(!_keepScores)
					_scores[idx] = new ScoreData();
			}
			else
			{
				transitionToScene("level");
				_state = GameState.SceneTransition;
			}
		}
	}

	private void playerScoresInput(int idx)
	{
		if(Input.GetKeyDown(_keymaps[idx].start) && !_contTimer.IsExpired)
		{
			transitionToScene("level");
			_state = GameState.SceneTransition;
			_keepScores = true;
		}
		else if(_contTimer.IsExpired)
		{
			_contTimer.Stop();
			transitionToScene("title");
			_state = GameState.SceneTransition;
			_keepScores = false;
		}
	}
	#endregion
	
	#region gameplay
	public void PlayerDied(PlayerBehaviour pm, int killerIdx)
	{
		_toRespawn.Enqueue(pm);

		//play sound for death at the location of the death
		GameObject ds = (GameObject)GameObject.Instantiate(deathSoundPrefab,
			                                               _players[pm.Index].Position,
		                                                   Quaternion.identity);
		ds.audio.clip = PlayerBehaviour.DeathSound;
		ds.audio.Play();

		
		//destroy the DeathSound GO when the sound is done
		GameObject.Destroy(ds, 2 * ds.audio.clip.length);
		
		//don't want suicide to count
		if(killerIdx != pm.Index) _scores[killerIdx].kills++;

		if(_scores[killerIdx].kills >= killsToEnd) //game over after n kills?
		{
			transitionToScene("scores");
			_state = GameState.SceneTransition;
		}
	}

	public void PlayerHit(int shooterIdx)
	{
		_scores[shooterIdx].bulletsHit++;
	}

	public void SpawnBullet(Vector3 position, Vector2 direction, int shooterIdx)
	{
		if(_bulletIdx >= _bullets.Length) //just to limit the number of times we need to use mod
			_bulletIdx%=_bullets.Length;

		_scores[shooterIdx].bulletsFired++;
		_bullets[_bulletIdx++].Spawn(position, direction, shooterIdx, _players[shooterIdx].gameObject.GetComponent<SpriteRenderer>().color);
	}
	#endregion

	//consider pushing this off into its own class
	#region level_setup
	private void transitionToScene(string name)
	{
		if(!Application.isLoadingLevel)
		{
			if(!Application.loadedLevelName.Equals(name))
				Application.LoadLevel(name);
		}
	}
	
	private void populateNewLevel()
	{
		_dim = buildLevel(_curLevelIdx++);
		if(_curLevelIdx >= levelNames.Length)
			_curLevelIdx %= levelNames.Length;

		createBullets();

		_mainCam.transform.position = new Vector3(_dim.x/2, -_dim.y/2, -1f);
		CameraBehaviour camB = _mainCam.gameObject.GetComponent<CameraBehaviour>();
		camB.toTrack = new Transform[_players.Length];
		camB.targetPos = new Vector3(_dim.x/2, -_dim.y/2, -1f);

		//player spawning
		for(int i=0;i<playerPrefabs.Length;++i)
		{
			spawnPlayer(i, camB);
			if(_spwnIdx == _spawnPoints.Length)
				resetSpawnPoints();
		}

		_toRespawn = new Queue<PlayerBehaviour>();
		_state = GameState.PlayGame;
	}

	/// <summary>
	/// Creates the bullets.
	/// </summary>
	private void createBullets()
	{
		_bullets = new BulletManager[50];
		for(int i=0;i<50;++i)
		{
			_bullets[i] = new BulletManager(bulletPrefab, i);
			_bullets[i].Disable();
		}
		_bulletIdx = 0;
	}

	/// <summary>
	/// Builds the level.
	/// </summary>
	/// <description>
	/// When it comes time to load a level, this will import it from the OGMO XML file
	/// and setup camera, spawn points, etc.  This does not spawn players.
	/// </description>
	private Vector2 buildLevel(int idx)
	{
		//spawn level
		GameObject sp = new GameObject("SpawnPoints");
		GameObject t = new GameObject("Tiles");
		Vector2 dim = ImportOGMOLevel.Import("Levels/" + levelNames[idx], tiles, t.transform, sp.transform);
		GameObject[] gos = GameObject.FindGameObjectsWithTag("SpawnPoint");
		_spawnPoints = new Vector3[gos.Length];
		for(int i=0;i<gos.Length;++i)
			_spawnPoints[i] = gos[i].transform.position;
		resetSpawnPoints();
		return dim;
	}

	/// <summary>
	/// Shuffles spawn points and sets the current index to 0.
	/// </summary>
	private void resetSpawnPoints()
	{
		for(int i=_spawnPoints.Length-1;i>=1;--i)
		{
			Vector3 tmp = _spawnPoints[i];
			int idx = Random.Range(0, i-1);
			_spawnPoints[i] = _spawnPoints[idx];
			_spawnPoints[idx] = tmp;
		}
		_spwnIdx = 0;
	}
	#endregion

	#region spawn_respawn
	/// <summary>
	/// Respawns the players by clearing the respawn queue.
	/// </summary>
	private void respawnPlayers()
	{
		while(_toRespawn.Count > 0 && _toRespawn.Peek().TimeOfDeath + respawnPenalty >= Time.time)
		{
			_toRespawn.Dequeue().Respawn(_spawnPoints[_spwnIdx++]);
			if(_spwnIdx >= _spawnPoints.Length)
				resetSpawnPoints();
		}
	}

	/// <summary>
	/// Spawns the player.
	/// </summary>
	/// <param name="idx">Index.</param>
	/// <param name="camB">Camera behaviour object.</param>
	private void spawnPlayer(int idx, CameraBehaviour camB)
	{
		if(_players[idx] != null)
			GameObject.DestroyImmediate(_players[idx].gameObject);

		GameObject go = (GameObject)GameObject.Instantiate(playerPrefabs[idx], _spawnPoints[_spwnIdx++], Quaternion.identity);
		_players[idx] = go.GetComponent<PlayerBehaviour>();
		_players[idx].Init(idx, !_started[idx]);
		camB.toTrack[idx] = go.transform;
	}
	#endregion
	
	#region properties
	public GameState State { get { return _state; } }
	public PlayerBehaviour[] Players { get { return _players; } }
	public ScoreData[] Scores { get { return _scores; } }
	#endregion
}
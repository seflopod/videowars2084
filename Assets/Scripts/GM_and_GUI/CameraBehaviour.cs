﻿using UnityEngine;
using System.Collections;

public class CameraBehaviour : MonoBehaviour
{
	public float minSize = 2f;
	public Transform[] toTrack;
	public Vector3 targetPos = Vector3.zero;
	public float positionSpeed = 5f;
	public float sizeSpeed = 2f;

	private Vector3[] _trackPos = new Vector3[0];
	private float _targetSize = 5f;

	//Track players and adjust camera to zoom in/out based on positions
	private void FixedUpdate()
	{
		transform.position = Vector3.Lerp(transform.position, targetPos, positionSpeed * Time.fixedDeltaTime);
		gameObject.camera.orthographicSize = Mathf.Lerp(gameObject.camera.orthographicSize, _targetSize, sizeSpeed * Time.fixedDeltaTime);
	}

	private void LateUpdate()
	{
		adjustPosition();
	}

	private void adjustPosition()
	{
		if(toTrack.Length < 1)
			return;

		if(_trackPos.Length != toTrack.Length)
		{
			Vector3[] tmp = new Vector3[toTrack.Length];
			for(int i=0;i<_trackPos.Length;++i)
				tmp[i] = _trackPos[i];
			for(int j=_trackPos.Length;j<toTrack.Length; ++j)
				tmp[j] = toTrack[j].position;

			_trackPos = tmp;
		}

		for(int i=0;i<_trackPos.Length;++i)
		{
			if(toTrack[i] == null || !toTrack[i].gameObject.activeInHierarchy)
				continue;

			if((toTrack[i].position-_trackPos[i]).sqrMagnitude >= 0.001)
				_trackPos[i] = toTrack[i].position;
		}

		float minX=100000f, minY=100000f, maxX=-100000f, maxY=-100000f;
		for(int i=0;i<_trackPos.Length;++i)
		{
			minX = Mathf.Min(_trackPos[i].x, minX);
			minY = Mathf.Min(_trackPos[i].y, minY);
			maxX = Mathf.Max(_trackPos[i].x, maxX);
			maxY = Mathf.Max(_trackPos[i].y, maxY);
		}
		
		if(minX == 100000 || minY == 100000f)
			return;
		float avgY = (maxY + minY) / 2;
		float avgX = (maxX + minX) / 2;
		Vector3 pos = new Vector3(avgX, avgY,  -1f);
		float size = Mathf.Max(Mathf.Abs(maxY-minY) / 2, Mathf.Abs(maxX-minX) / (2 * camera.aspect)) + 1.28f;

		_targetSize = Mathf.Max (minSize, size);
		targetPos = pos;
	}

}

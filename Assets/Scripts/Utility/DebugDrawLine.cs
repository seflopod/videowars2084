﻿using UnityEngine;
using System.Collections;

public class DebugDrawLine : MonoBehaviour
{
	private static Material lineMaterial;

	private static void CreateLineMaterial() {
		if( !lineMaterial ) {
			lineMaterial = new Material( "Shader \"Lines/Colored Blended\" {" +
			                            "SubShader { Pass { " +
			                            "    Blend SrcAlpha OneMinusSrcAlpha " +
			                            "    ZWrite Off Cull Off Fog { Mode Off } " +
			                            "    BindChannels {" +
			                            "      Bind \"vertex\", vertex Bind \"color\", color }" +
			                            "} } }" );
			lineMaterial.hideFlags = HideFlags.HideAndDontSave;
			lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;
		}
	}
	public static Transform[] starts = new Transform[4];
	public static Transform[] ends = new Transform[4];

	private void OnPostRender()
	{
		CreateLineMaterial();
		lineMaterial.SetPass(0);
		for(int i=0;i<4;++i)
		{
			if(starts[i] != null && ends[i] != null)
			{
				Vector3 start = starts[i].position;
				Vector3 end = ends[i].position;
				GL.Begin(GL.LINES);
				GL.Color(Color.white);
				GL.Vertex(start);
				GL.Vertex(end);
				GL.End();
			}
		}
	}


}

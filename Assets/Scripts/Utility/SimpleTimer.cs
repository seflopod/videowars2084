﻿using System.Diagnostics;

public class SimpleTimer
{
	private double _t; //duration in milliseconds
	private bool _paused;
	private bool _started;
	private Stopwatch _sw;

	public SimpleTimer() : this(0.0)
	{}

	public SimpleTimer(double duration)
	{
		_t = duration;
		_sw = new Stopwatch();
		_started = false;
		_paused = false;
	}

	public void Start()
	{
		if(_paused)
			_paused = false;

		_sw.Start();
		_started = true;
	}

	public void Pause()
	{
		_paused = true;
		_sw.Stop();
	}

	public void Stop()
	{
		_sw.Reset();
		_started = false;
	}

	/// <summary>
	/// Gets the time remaining in milliseconds.
	/// </summary>
	/// <value>The time remaining.</value>
	public double TimeRemaining
	{
		get
		{
			if(_paused || _started)
				return _t - _sw.Elapsed.TotalMilliseconds;

			return 0f;
		}
	}

	public bool IsExpired
	{
		get { return TimeRemaining <= 0; }
	}

	public bool Started { get { return _started; } }
	public bool Paused { get { return _paused; } }
	public bool Stopped { get { return !Started; } }

	/// <summary>
	/// Gets or sets the duration.
	/// </summary>
	/// <value>The duration in milliseconds.</value>
	public double Duration { get { return _t; } set { _t = value; } }
}
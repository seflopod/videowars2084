﻿using UnityEngine;

public class Keymap
{
	public readonly KeyCode up;
	public readonly KeyCode down;
	public readonly KeyCode left;
	public readonly KeyCode right;
	public readonly KeyCode button1;
	public readonly KeyCode button2;
	public readonly KeyCode button3;
	public readonly KeyCode button4;
	public readonly KeyCode button5;
	public readonly KeyCode button6;
	public readonly KeyCode button7;
	public readonly KeyCode coin;
	public readonly KeyCode start;
	
	public Keymap() : this(1)
	{}

	public Keymap(int setNum)
	{
		if(setNum > 2 || setNum < 1)
			setNum = 1;
		
		switch(setNum)
		{
		case 1:
			up = KeyCode.W;
			down = KeyCode.S;
			left = KeyCode.A;
			right = KeyCode.D;
			button1 = KeyCode.U;
			button2 = KeyCode.I;
			button3 = KeyCode.O;
			button4 = KeyCode.J;
			button5 = KeyCode.K;
			button6 = KeyCode.L;
			button7 = KeyCode.Space;
			coin = KeyCode.Backslash;
			start = KeyCode.Return;
			break;
		case 2:
			up = KeyCode.UpArrow;
			down = KeyCode.DownArrow;
			left = KeyCode.LeftArrow;
			right = KeyCode.RightArrow;
			button1 = KeyCode.Keypad4;
			button2 = KeyCode.Keypad5;
			button3 = KeyCode.Keypad6;
			button4 = KeyCode.Keypad1;
			button5 = KeyCode.Keypad2;
			button6 = KeyCode.Keypad3;
			button7 = KeyCode.Keypad0;
			coin = KeyCode.KeypadPlus;
			start = KeyCode.KeypadPeriod;
			break;
		default:
			break;
		}
	}
}
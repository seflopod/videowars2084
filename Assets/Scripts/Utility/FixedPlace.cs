﻿using UnityEngine;
using System.Collections;

public class FixedPlace : MonoBehaviour
{

	private Vector3 _pos;

	private void Start()
	{
		_pos = transform.position;
	}

	private void LateUpdate()
	{
		if(transform.position != _pos)
			transform.position = _pos;
	}
}

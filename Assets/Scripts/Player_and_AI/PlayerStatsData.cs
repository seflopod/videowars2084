﻿using UnityEngine;

public class PlayerStatsData
{
	public int health = 1000;
	public int maxHealth = 1000;
	public int fuelRemaining = 3000;
	public int maxFuel = 3000;
	public int fuelCost = 100;
	public float fuelRegenTime = 5.0f;
	public int ammoRemaining = 5000;
	public int maxAmmo = 5000;
	public float rateOfFire = 5f; //shots per second
	public float reloadTime = 1.5f;
	public float jumpSpeed = 2.5f;
	public float jumpResetTime = 1.0f;
	public float moveSpeed = 5f;
	public float thrustForce = 200.0f;
}

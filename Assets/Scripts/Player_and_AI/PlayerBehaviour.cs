﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerBehaviour : MonoBehaviour
{

	#region util_classes
	public class StateBools
	{
		public bool inGame;
		public bool canJump;
		public bool canShoot;
		public bool canThrust;
		public bool reloading;
		public bool fireCooldown;
		public bool thrusting;
		public bool fullThrustRegen;
		public bool dead;
		
		public StateBools()
		{
			Reset();
		}
		
		public void Reset()
		{
			inGame = false;
			canJump = true;
			canShoot = true;
			canThrust = true;
			reloading = false;
			fireCooldown = false;
			thrusting = false;
			fullThrustRegen = false;
			dead = false;
		}
	};
	#endregion
	
	#region statics
	public static AudioClip DamageSound { get; set; }
	public static AudioClip DeathSound { get; set; }
	public static AudioClip FireSound { get; set; }
	#endregion
	
	#region fields
	private GameObject _playerObj;
	private Rigidbody2D _playerBody;
	
	private Vector2 _moveDirection;
	//private Vector2 _fireDirection;
	
	private SimpleTimer _fuelRegen;
	private SimpleTimer _fireTimer;
	private SimpleTimer _reloadTimer;
	private SimpleTimer _jumpTimer;
	
	//ai-related
	/*private SimpleTimer _moveTimer;
	private int _curTarget;
	private Collider2D[] _hitColliders;*/
	private bool _isAI;
	private AIStateMachine _ai;

	#endregion
	
	#region init
	public void Init(int idx, bool asAI)
	{
		Index = idx;
		IsAI = asAI;
		State.inGame = true;
	}
	#endregion

	#region monobehaviour
	private void Awake()
	{
		_playerObj = gameObject;
		_playerBody = gameObject.rigidbody2D;

		Stats = new PlayerStatsData();
		State = new StateBools();
		
		_moveDirection = Vector2.zero;
		//_fireDirection = Vector2.right;
		
		_fuelRegen = new SimpleTimer(Stats.fuelRegenTime);
		_fireTimer = new SimpleTimer(1000.0/Stats.rateOfFire);
		_reloadTimer = new SimpleTimer(Stats.reloadTime);
		_jumpTimer = new SimpleTimer(Stats.jumpResetTime);
		
		_playerObj.GetComponent<Animator>().SetFloat("healthPct", 1.0f);
		State.inGame = false;
		_ai = null;
		IsAI = true;
	}

	private void LateUpdate()
	{
		CheckTimers();
	}
	#endregion

	#region utility
	public void Respawn(Vector3 spawnPos)
	{
		gameObject.SetActive(true);
		gameObject.transform.position = spawnPos;

		_moveDirection = Vector2.zero;
		//_fireDirection = Vector2.right;
		
		_fuelRegen.Stop();
		_fuelRegen.Duration = Stats.fuelRegenTime;
		_fireTimer.Stop();
		_fireTimer.Duration = 1000.0/Stats.rateOfFire;
		_reloadTimer.Stop();
		_reloadTimer.Duration = Stats.reloadTime;
		_jumpTimer.Stop();
		_jumpTimer.Duration = Stats.jumpResetTime;

		Stats = new PlayerStatsData();
		State.Reset();

		_playerObj.GetComponent<Animator>().SetFloat("healthPct", 1.0f);

		State.inGame = true;
	}
	
	public void CheckTimers()
	{
		if(_fuelRegen.Started)
		{
			if(_fuelRegen.IsExpired)
			{
				State.canThrust = true;
				Stats.fuelRemaining = Stats.maxFuel;
				_fuelRegen.Stop();
			}
			else
			{
				Stats.fuelRemaining += Stats.maxFuel/(int)Mathf.Max((float)_fuelRegen.Duration, 10f);
			}
		}
		
		if(State.reloading && _reloadTimer.Started && _reloadTimer.IsExpired)
		{
			State.reloading = false;
			State.canShoot = true;
			Stats.ammoRemaining = Stats.maxAmmo;
			_reloadTimer.Stop();
		}
		
		if(State.fireCooldown && _fireTimer.IsExpired)
		{
			State.fireCooldown = false;
			State.canShoot = true;
			_fireTimer.Stop();
		}

		if(!State.canJump && _jumpTimer.IsExpired)
		{
			State.canJump = true;
			_jumpTimer.Stop();
		}
	}
	#endregion
	
	#region movement
	public void Move(Vector2 dir)
	{
		if(dir.y != 0 && !State.canJump) dir.y = 0f;
		if(dir.y != 0 && !_jumpTimer.Started) _jumpTimer.Start();
		
		_moveDirection = dir;
		
		Vector2 velNorm = _playerBody.velocity.normalized;
		_moveDirection = (velNorm + dir).normalized;

		float curSpeedSq = _playerBody.velocity.sqrMagnitude;
		if(velNorm != _moveDirection || curSpeedSq < Stats.moveSpeed * Stats.moveSpeed)
		{
			_playerBody.velocity = _playerBody.velocity + _moveDirection * Stats.moveSpeed;
			if(_playerBody.velocity.sqrMagnitude > Stats.moveSpeed*Stats.moveSpeed)
				_playerBody.velocity = _moveDirection * Stats.moveSpeed;
		}
	}
	
	public void Stop()
	{
		Vector2 vel = _playerBody.velocity;
		vel.x *= 0.4f;
		
		//if the player is moving up the reduction is less than if they are
		//moving down.
		vel.y = (vel.y > 0.0f) ? vel.y*0.8f : vel.y*1.001f;
		_playerBody.velocity = vel;
	}
	
	public void Thrust(float dt)
	{
		if(State.canThrust)
		{
			if(!State.fullThrustRegen && Stats.fuelRemaining >= Stats.fuelCost)
			{
				Vector2 force = 2 * Stats.thrustForce * _moveDirection;
				_playerBody.AddForce(force);
				Stats.fuelRemaining -= Stats.fuelCost;
				State.thrusting = true;
			}
			
			if(Stats.fuelRemaining <= Stats.maxFuel)
			{
				State.canThrust = false;
				State.thrusting = false;
				State.fullThrustRegen = true;
				_fuelRegen.Duration = 5f;
				_fuelRegen.Start();
			}
		}
	}
	
	public void EndThrust()
	{
		State.thrusting = false;
		if(!State.fullThrustRegen)
		{
			_fuelRegen.Duration = 0f;
			_fuelRegen.Start();
		}
	}
	#endregion
	
	#region shooting
	public void Fire(bool shootFwd)
	{
		if(State.canShoot)
		{
			Vector3 pos = _playerObj.transform.position;
			Vector2 dPos = _playerBody.velocity * Time.fixedDeltaTime;
			Vector2 bcSize = _playerObj.GetComponent<BoxCollider2D>().size/2;
			Vector3 offset = new Vector3(_moveDirection.x * (bcSize.x + 0.125f + dPos.x), _moveDirection.y * (bcSize.y + 0.125f + dPos.y), 0f);
			if(shootFwd)
				pos += offset;
			else
				pos -= offset;

			Vector2 fireDirection = ((shootFwd)?1:-1)*_playerBody.velocity.normalized;
			//tell the game to spawn the bullet at the proper position
			GameManager.Instance.SpawnBullet(pos, fireDirection, Index);

			Stats.ammoRemaining--;
			
			//check to see if we need to reload or just put firing on cooldown
			if(Stats.ammoRemaining <= 0)
			{
				Stats.ammoRemaining = 0;
				_reloadTimer.Start();
				_fireTimer.Stop();
				State.fireCooldown = false;
				State.reloading = true;
			}
			else
			{
				_fireTimer.Start();
				State.fireCooldown = true;
			}
			
			//no matter what, can't shoot for a time
			State.canShoot = false;

			//play audio
			_playerObj.audio.clip = FireSound;
			_playerObj.audio.Play();
		}
	}
	
	public void TakeDamage(int dmg, int shooterIdx)
	{
		Stats.health -= dmg;
		
		//set the animation based on the amount of health left
		float hPct = Stats.health/(float)Stats.maxHealth;
		_playerObj.GetComponent<Animator>().SetFloat("healthPct", hPct);
		
		//play sound for damage
		_playerObj.audio.clip = DamageSound;
		_playerObj.audio.Play();
		GameManager.Instance.PlayerHit(shooterIdx);
		if(Stats.health <= 0)
		{   //dead
			State.dead = true;
			//State.inGame = false;
			Stats.health = 0;
			TimeOfDeath = Time.time;
			GameManager.Instance.PlayerDied(this, shooterIdx);
			_playerObj.rigidbody2D.velocity = Vector3.zero;
			_playerObj.audio.Stop();
			_playerObj.SetActive(false);
		}
	}
	#endregion

	#region properties
	public int Index { get; set; }
	public PlayerStatsData Stats { get; set; }
	public StateBools State { get; private set; }

	public bool IsAI
	{
		get { return _isAI; }
		set
		{
			if(value && _ai != null) _ai.enabled = true;
			if(value && _ai == null) _ai = _playerObj.AddComponent<AIStateMachine>();
			if(!value && _ai != null) _ai.enabled = false;

			_isAI = value;
		}
	}

	public float TimeOfDeath { get; private set; }
	public Vector3 Position
	{
		get { return transform.position; }
		set { transform.position = value; }
	}
	public GameObject PlayerObj { get { return _playerObj; } }
	#endregion
}
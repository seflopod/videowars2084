﻿using UnityEngine;
using System.Collections.Generic;

public enum AIStates
{
	NoTarget = 0x01,
	HaveTarget = 0x02
};

/*
 * ai state machine
 * 
 * state: NO TARGET
 * 
 * if Move_Timer has expired, pick a direction to move in, set Move_Timer to [0.25s, 1s]
 * move in Move_Direction
 * look for target
 * if valid target is found, state: HAVE TARGET
 * 
 * 
 * state: HAVE TARGET
 * 
 * move to target
 * if target is in range and unobstructed, fire
 * if target is obstructed for Obstruction_Timer, state: NO TARGET, set Move_Timer to 0
 * if target is not in range, state: NO TARGET, set Move_Timer to 0
 * 
 */
[RequireComponent(typeof(PlayerBehaviour))]
public class AIStateMachine : MonoBehaviour
{
	private static int _playerLayerMask = 1 << LayerMask.NameToLayer("Players");

	public float range = 6f;
	public float minMoveTimer = 250f; //in ms
	public float maxMoveTimer = 1000f; //in ms
	public float obstructionTimeout = 500f; //in ms
	
	private SimpleTimer _obstructionTimer;
	private SimpleTimer _moveTimer;
	private Transform _target;
	private Vector3 _toTarget;
	private PlayerBehaviour _player;
	private Vector2 _moveDirection;
	private Collider2D[] _hitColliders;
	private bool _started = false;

	private void Start()
	{
		_obstructionTimer = new SimpleTimer(obstructionTimeout);
		_moveTimer = new SimpleTimer(0.0);
		_target = null;
		_toTarget = Vector3.zero;
		_player = gameObject.GetComponent<PlayerBehaviour>();
		_moveDirection = Vector2.zero;
		_hitColliders = new Collider2D[GameManager.Instance.playerPrefabs.Length];
		_started = true;
		State = AIStates.NoTarget;
	}

	private void Update()
	{
		switch(State)
		{
		case AIStates.NoTarget:
			DebugDrawLine.starts[_player.Index] = null;
			doNoTargetMove();
			break;
		case AIStates.HaveTarget:
			if(_target != null)
			{
				Vector3 vel = new Vector3(_target.rigidbody2D.velocity.x, _target.rigidbody2D.velocity.y, 0f);
				_toTarget = _target.position - transform.position - vel*Time.deltaTime;
				//DebugDrawLine.starts[_player.Index] = transform;
				//DebugDrawLine.ends[_player.Index] = _target;
				doHaveTargetMove();
				if(_target.gameObject.GetComponent<PlayerBehaviour>().State.dead)
				{
					State = AIStates.NoTarget;
					_moveTimer.Duration = 0.0;
				}
			}
			else
				State = AIStates.NoTarget;
			break;
		default:
			break;
		}
	}

	private void FixedUpdate()
	{
		switch(State)
		{
		case AIStates.NoTarget:
			_target = seekTarget();
			if(_target != null) State = AIStates.HaveTarget;
			break;
		case AIStates.HaveTarget:
			bool obs = targetObstructed;
			bool rng = targetInRange;

			if(obs && _obstructionTimer.Stopped)
			{
				_obstructionTimer.Duration = (double)obstructionTimeout;
				_obstructionTimer.Start();
			}

			if(rng && !obs)
			{
				_obstructionTimer.Stop();
				_player.Fire(true); //need to determine how ai shoots backward
			}

			if(!rng || (_obstructionTimer.IsExpired && _obstructionTimer.Started))
			{
				State = AIStates.NoTarget;
				_moveTimer.Duration = 0.0;
			}
			break;
		default:
			break;
		}
	}

	private void OnEnable()
	{
		if(_started)
		{
			_obstructionTimer.Duration = obstructionTimeout;
			_obstructionTimer.Stop();
			_moveTimer.Duration = 0.0;
			_moveTimer.Start();
			_target = null;
			_toTarget = Vector3.zero;
			_moveDirection = Vector2.zero;
			State = AIStates.NoTarget;
		}
	}

	private void OnCollisionEnter2D(Collision2D c)
	{
		if(_started && c.collider != null)
		{
			GameObject t = c.collider.gameObject;
			Vector3 rel3 = (t.transform.position - transform.position).normalized;
			Vector2 rel = new Vector2(rel3.x, rel3.y);
			if(t.CompareTag("Tile") && rel.normalized == _moveDirection)
				_moveTimer.Duration = 0.0;
		}
	}

	#region no_target_methods
	private void doNoTargetMove()
	{
		if(_moveTimer.IsExpired)
		{
			_moveTimer.Stop();
			_moveDirection = pickDirection();
			_moveTimer.Duration = Random.Range(minMoveTimer, maxMoveTimer);
			_moveTimer.Start();
		}
		_player.Move(_moveDirection);
	}

	private Vector2 pickDirection()
	{

		Vector2 dir = Vector3.zero;
		Vector2 origin = new Vector2(transform.position.x, transform.position.y);
		RaycastHit2D hit = new RaycastHit2D();
		bool[] tried = new bool[8]{false, false, false, false, false, false, false, false};
		while(hit.collider == null)
		{
			int aiDir = Random.Range(0, 8);
			while(tried[aiDir])
				aiDir = Random.Range(0, 8);
			tried[aiDir] = true;
			int triedCtr = 0;
			for(int i=0;i<8;++i)
				if(tried[i]) triedCtr++;
			if(triedCtr>=8)
				break;
			dir = new Vector2(Mathf.Cos(aiDir*Mathf.PI/4), Mathf.Sin(aiDir*Mathf.PI/4));
			hit = Physics2D.Raycast(origin, dir, range, (1 << LayerMask.NameToLayer("Tiles")));
		}
		return dir;
	}

	private Transform seekTarget()
	{
		Vector2 origin = new Vector2(transform.position.x, transform.position.y);
		float r = range/100f * gameObject.GetComponent<SpriteRenderer>().sprite.rect.width;
		int nHits = Physics2D.OverlapCircleNonAlloc(origin, r, _hitColliders, _playerLayerMask);

		List<GameObject> targets = new List<GameObject>();
		if(nHits > 0 )
		{
			origin = new Vector2(transform.position.x, transform.position.y);
			Vector2 pos = new Vector2(transform.position.x, transform.position.y);
			RaycastHit2D hit;
			for(int i=0; i<nHits; ++i)
			{
				GameObject target = _hitColliders[i].gameObject;
				Vector2 toT = (new Vector2(target.transform.position.x, target.transform.position.y)) - pos;
				hit = Physics2D.Raycast(origin, toT.normalized, toT.magnitude, (1 << LayerMask.NameToLayer("Tiles")));
				if(hit.collider != null) //if you can't see it, you can't target it
					continue;

				int tIdx = target.GetComponent<PlayerBehaviour>().Index;
				
				if(tIdx == _player.Index) //don't want to target self
					continue;
				
				targets.Add(target);
			}
		}
		
		if(targets.Count == 0)
		{
			return null;
		}
		else
		{
			//this will randomly pick a target
			//it should be based on target health plus
			//distance in some fashion
			int idx = Random.Range(0, targets.Count);
			return targets[idx].transform;
		}
	}
	#endregion

	#region have_target_methods
	private void doHaveTargetMove()
	{
		if(_moveTimer.IsExpired)
		{
			_moveTimer.Stop();
			_moveDirection = (new Vector2(_toTarget.x, _toTarget.y)).normalized;
			_moveTimer.Duration = minMoveTimer;
			_moveTimer.Start();
		}
		_player.Move(_moveDirection);
	}

	private bool targetInRange
	{
		get { return _toTarget.sqrMagnitude <= range * range; }
	}

	private bool targetObstructed
	{
		get
		{
			Vector2 origin = new Vector2(transform.position.x, transform.position.y);
			RaycastHit2D hit = Physics2D.Raycast(origin, _moveDirection, _toTarget.magnitude, (1 << LayerMask.NameToLayer("Tiles")));
			return hit.collider != null;
		}
	}
	#endregion

	public AIStates State { get; private set; }
}

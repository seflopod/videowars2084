﻿using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{	
	public BulletManager manager;

	private SimpleTimer _reset;

	private void Awake()
	{
		_reset = new SimpleTimer(1/60f);
	}

	private void Update()
	{
		if(_reset.Stopped || _reset.IsExpired)
		//if too slow, just deactivate
		if(rigidbody2D.velocity.sqrMagnitude < 25.0f)
			gameObject.SetActive(false);
	}

	private void LateUpdate()
	{
		if(_reset.Started && _reset.IsExpired)
			_reset.Stop();
	}

	/// <summary>
	/// Handles the collision enter event.
	/// </summary>
	/// <param name="c">The collision</param>
	/// <description>
	/// This will make the Player on the other end of the collision (if there is a Player) take
	/// damage proportional to the velocity of the bullet.
	/// </description>
	private void OnCollisionEnter2D(Collision2D c)
	{
		PlayerBehaviour pb = c.gameObject.GetComponent<PlayerBehaviour>();
		if(pb != null)
		{
			if(manager == null)
				Debug.LogError("wtf?");
			pb.TakeDamage(Mathf.FloorToInt(pb.Stats.maxHealth/5f), manager.ShooterIdx);
			gameObject.SetActive(false);
		}
	}

	public void Reset()
	{
		_reset.Start();
	}
}
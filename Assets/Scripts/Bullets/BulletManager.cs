﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Bullet manager.
/// </summary>
public class BulletManager
{
	private int _id;
	private GameObject _bulletPrefab;
	private GameObject _bulletObj;

	/// <summary>
	/// Initializes a new instance of the <see cref="BulletManager"/> class.
	/// </summary>
	/// <param name="bulletPrefab">Bullet prefab.</param>
	/// <param name="id">Identifier.</param>
	public BulletManager(GameObject bulletPrefab, int id)
	{
		_bulletPrefab = bulletPrefab;
		_bulletObj = null;
		_id = id;
	}

	/// <summary>
	/// Spawn the specified pos, velDir and shooterIdx.
	/// </summary>
	/// <param name="pos">Position.</param>
	/// <param name="velDir">Velocity direction.</param>
	/// <param name="shooterIdx">The index of the shooter (PlayerManager) in the GameManager players array</param>
	/// <description>
	/// This will place the bullet gameObject associated with this manager at the specified
	/// position with the specified speed.  If there is no such gameObject yet, then it is
	/// instantiated.
	/// </description>
	public void Spawn(Vector3 pos, Vector2 velDir, int shooterIdx, Color color)
	{
		if(_bulletObj == null)
		{
			_bulletObj = (GameObject)Object.Instantiate(_bulletPrefab, pos, Quaternion.identity);
			_bulletObj.GetComponent<BulletBehaviour>().manager = this;
		}
		//color.a = 1.0f;
		//_bulletObj.GetComponent<SpriteRenderer>().color = color;


		_bulletObj.SetActive(true);
		_bulletObj.GetComponent<BulletBehaviour>().Reset();
		_bulletObj.transform.position = pos;
		_bulletObj.rigidbody2D.velocity = velDir * 20.0f;
		_bulletObj.GetComponent<SpriteRenderer>().color = color;
		ShooterIdx = shooterIdx;
	}

	/// <summary>
	/// Disable this instance.
	/// </summary>
	public void Disable()
	{
		if(_bulletObj != null)
			_bulletObj.SetActive(false);
	}

	/// <summary>
	/// Enable this instance.
	/// </summary>
	public void Enable()
	{
		if(_bulletObj != null)
			_bulletObj.SetActive(true);
	}

	/// <summary>
	/// Gets the identifier.
	/// </summary>
	/// <value>The identifier.</value>
	public int Id { get { return _id; } }

	/// <summary>
	/// Gets or sets the index of the shooter.
	/// </summary>
	/// <value>The index of the shooter.</value>
	public int ShooterIdx { get; set; }
}

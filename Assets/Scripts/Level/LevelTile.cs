﻿using UnityEngine;
using System.Collections.Generic;

public class LevelTile
{
	private float _x;
	private float _y;
	
	private bool _physics;
	
	public LevelTile()
	{
		_x = -1f;
		_y = -1f;
		Exists = false;
		_physics = false;
	}
	
	public LevelTile(float worldX, float worldY)
	{
		_x = worldX;
		_y = worldY;
		_physics = false;
		Exists = true;
	}
	
	public float X { get { return _x; } }
	public float Y { get { return _y; } }
	public bool UsePhysics
	{
		get { return _physics; }
		set { _physics = value; }
	}
	public bool Exists { get; private set; }
}

﻿using UnityEngine;

[System.Serializable()]
public class Tileset
{
	public GameObject flatPlatform;
	public GameObject slope;
}

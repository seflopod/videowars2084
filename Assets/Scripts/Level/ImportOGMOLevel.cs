﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Collections.Generic;

public class ImportOGMOLevel
{
	public static Vector2 Import(string resourceLocStr, Tileset tileset, Transform tileParent, Transform spParent)
	{
		TextAsset xmlTA = Resources.Load<TextAsset>(resourceLocStr);
		string xmlStr = xmlTA.text;
		LevelTile[,] tiles;

		int rows,cols;
		float width, height;

		using(XmlReader reader = XmlReader.Create(new StringReader(xmlStr)))
		{
			reader.ReadToFollowing("level");
			width = float.Parse(reader.GetAttribute("width"));
			height = float.Parse (reader.GetAttribute("height"));
			rows = Mathf.FloorToInt(height)/64;
			cols = Mathf.FloorToInt(width)/64;
			width /= 100;
			height /= 100;

			//first collect the spawn points and put them in the scene
			reader.ReadToFollowing("SpawnPoints");
			reader.ReadToFollowing("spawnPoint");
			do
			{
				float x = float.Parse(reader.GetAttribute("x"))/100f;
				float y = float.Parse(reader.GetAttribute("y"))/-100f;
				GameObject go = new GameObject("spawnPoint("+x+","+y+")");
				go.transform.position = new Vector3(x, y, 0f);
				go.tag = "SpawnPoint";
				go.transform.parent = spParent;
			}while(reader.ReadToNextSibling("spawnPoint"));

			//next bring in the tiles, no gameobjects made until the collisions are imported
			tiles = new LevelTile[rows, cols];
			for(int r=0;r<rows;++r)
				for(int c=0;c<cols;++c)
					tiles[r,c] = new LevelTile();


			if(!reader.ReadToFollowing("Tiles"))
				Debug.LogError("No tiles?");
			reader.ReadToFollowing("tile");
			do
			{
				int tileX = int.Parse(reader.GetAttribute("x"));
				int tileY = int.Parse(reader.GetAttribute("y"));
				tiles[tileY,tileX] = new LevelTile(0.64f*tileX, -0.64f*tileY);
			}while(reader.ReadToNextSibling("tile"));
		}

		//instantiate level
		for(int r=0;r<rows;++r)
		{
			for(int c=0;c<cols;++c)
			{
				if(tiles[r,c].Exists)
				{
					LevelTile lt = tiles[r,c];
					GameObject go = (GameObject)GameObject.Instantiate(tileset.flatPlatform, new Vector3(lt.X, lt.Y, 0f), Quaternion.identity);
					go.name = "tile("+lt.X+","+lt.Y+")";
					//go.tag = "Tile";
					go.transform.parent = tileParent;
				}
			}
		}

		//add colliders
		GameObject cGO = new GameObject("collider_parent");
		cGO.transform.position = Vector3.zero;
		//this pass collapses colliders into horizontal rows where appropriate
		for(int r=0;r<rows;++r)
		{
			int c=0;
			while(c < cols)
			{
				int tW = 0;
				float lx = (tiles[r,c].Exists)? tiles[r,c].X : 0f;
				float ly = (tiles[r,c].Exists)? tiles[r,c].Y : 0f;

				while(c < cols && tiles[r,c++].Exists)
					tW+=1;

				if(tW >= 1)
				{
					float w = tW * 0.64f;
					float h = 0.64f;
					float x = lx + w/2 - 0.32f; //the first tile was already centered, fix for that
					float y = ly;

					GameObject cObj = new GameObject("collision_object", typeof(Rigidbody2D), typeof(BoxCollider2D));

					//position the new collision object
					cObj.transform.position = new Vector3(x, y, 0f);
					cObj.transform.parent = cGO.transform;
					cObj.tag = "Tile";
					cObj.layer = LayerMask.NameToLayer("Tiles");

					//setup the rigidbody
					Rigidbody2D rBody = cObj.GetComponent<Rigidbody2D>();
					rBody.gravityScale = 0f;
					rBody.isKinematic = true;

					//fix the collider to the right dimensions
					BoxCollider2D box = cObj.GetComponent<BoxCollider2D>();
					box.center = Vector2.zero;
					box.size = new Vector2(w, h);
				}
			}
		}

		//return the level dimensions
		return new Vector2(width, height);
	}
}

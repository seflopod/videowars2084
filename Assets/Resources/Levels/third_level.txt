<level width="1680" height="1050">
  <camera x="0" y="0" />
  <SpawnPoints>
    <spawnPoint id="0" x="96" y="96" />
    <spawnPoint id="1" x="192" y="224" />
    <spawnPoint id="2" x="1536" y="192" />
    <spawnPoint id="3" x="1504" y="384" />
    <spawnPoint id="4" x="1440" y="416" />
    <spawnPoint id="5" x="1424" y="672" />
    <spawnPoint id="6" x="1488" y="864" />
    <spawnPoint id="7" x="1296" y="896" />
    <spawnPoint id="8" x="1040" y="896" />
    <spawnPoint id="9" x="976" y="800" />
    <spawnPoint id="10" x="544" y="800" />
    <spawnPoint id="11" x="336" y="864" />
    <spawnPoint id="12" x="688" y="896" />
    <spawnPoint id="13" x="816" y="800" />
    <spawnPoint id="14" x="176" y="800" />
    <spawnPoint id="15" x="80" y="896" />
    <spawnPoint id="16" x="128" y="576" />
    <spawnPoint id="17" x="240" y="416" />
    <spawnPoint id="18" x="352" y="192" />
    <spawnPoint id="19" x="448" y="128" />
    <spawnPoint id="20" x="736" y="192" />
    <spawnPoint id="21" x="1184" y="96" />
    <spawnPoint id="22" x="1248" y="256" />
    <spawnPoint id="23" x="432" y="512" />
    <spawnPoint id="24" x="528" y="416" />
    <spawnPoint id="25" x="624" y="544" />
    <spawnPoint id="26" x="624" y="704" />
    <spawnPoint id="27" x="800" y="608" />
    <spawnPoint id="28" x="736" y="480" />
    <spawnPoint id="29" x="1024" y="416" />
    <spawnPoint id="30" x="1168" y="480" />
    <spawnPoint id="31" x="1216" y="544" />
    <spawnPoint id="32" x="944" y="640" />
    <spawnPoint id="33" x="1088" y="704" />
    <spawnPoint id="34" x="880" y="448" />
    <spawnPoint id="35" x="1040" y="192" />
    <spawnPoint id="36" x="912" y="128" />
    <spawnPoint id="37" x="640" y="96" />
    <spawnPoint id="38" x="496" y="256" />
  </SpawnPoints>
  <Tiles tileset="Tileset01" exportMode="XMLCoords">
    <tile x="0" y="0" tx="0" ty="0" />
    <tile x="0" y="1" tx="0" ty="0" />
    <tile x="0" y="2" tx="0" ty="0" />
    <tile x="0" y="3" tx="0" ty="0" />
    <tile x="0" y="4" tx="0" ty="0" />
    <tile x="0" y="5" tx="0" ty="0" />
    <tile x="0" y="6" tx="0" ty="0" />
    <tile x="0" y="7" tx="0" ty="0" />
    <tile x="0" y="8" tx="0" ty="0" />
    <tile x="0" y="9" tx="0" ty="0" />
    <tile x="0" y="10" tx="0" ty="0" />
    <tile x="0" y="11" tx="0" ty="0" />
    <tile x="0" y="12" tx="0" ty="0" />
    <tile x="0" y="13" tx="0" ty="0" />
    <tile x="0" y="14" tx="0" ty="0" />
    <tile x="0" y="15" tx="0" ty="0" />
    <tile x="1" y="0" tx="0" ty="0" />
    <tile x="1" y="15" tx="0" ty="0" />
    <tile x="2" y="0" tx="0" ty="0" />
    <tile x="2" y="15" tx="0" ty="0" />
    <tile x="3" y="0" tx="0" ty="0" />
    <tile x="3" y="15" tx="0" ty="0" />
    <tile x="4" y="0" tx="0" ty="0" />
    <tile x="4" y="15" tx="0" ty="0" />
    <tile x="5" y="0" tx="0" ty="0" />
    <tile x="5" y="5" tx="0" ty="0" />
    <tile x="5" y="6" tx="0" ty="0" />
    <tile x="5" y="7" tx="0" ty="0" />
    <tile x="5" y="8" tx="0" ty="0" />
    <tile x="5" y="9" tx="0" ty="0" />
    <tile x="5" y="10" tx="0" ty="0" />
    <tile x="5" y="15" tx="0" ty="0" />
    <tile x="6" y="0" tx="0" ty="0" />
    <tile x="6" y="5" tx="0" ty="0" />
    <tile x="6" y="10" tx="0" ty="0" />
    <tile x="6" y="15" tx="0" ty="0" />
    <tile x="7" y="0" tx="0" ty="0" />
    <tile x="7" y="5" tx="0" ty="0" />
    <tile x="7" y="10" tx="0" ty="0" />
    <tile x="7" y="15" tx="0" ty="0" />
    <tile x="8" y="0" tx="0" ty="0" />
    <tile x="8" y="5" tx="0" ty="0" />
    <tile x="8" y="15" tx="0" ty="0" />
    <tile x="9" y="0" tx="0" ty="0" />
    <tile x="9" y="5" tx="0" ty="0" />
    <tile x="9" y="15" tx="0" ty="0" />
    <tile x="10" y="0" tx="0" ty="0" />
    <tile x="10" y="5" tx="0" ty="0" />
    <tile x="10" y="15" tx="0" ty="0" />
    <tile x="11" y="0" tx="0" ty="0" />
    <tile x="11" y="5" tx="0" ty="0" />
    <tile x="11" y="15" tx="0" ty="0" />
    <tile x="12" y="0" tx="0" ty="0" />
    <tile x="12" y="5" tx="0" ty="0" />
    <tile x="12" y="15" tx="0" ty="0" />
    <tile x="13" y="0" tx="0" ty="0" />
    <tile x="13" y="5" tx="0" ty="0" />
    <tile x="13" y="15" tx="0" ty="0" />
    <tile x="14" y="0" tx="0" ty="0" />
    <tile x="14" y="5" tx="0" ty="0" />
    <tile x="14" y="15" tx="0" ty="0" />
    <tile x="15" y="0" tx="0" ty="0" />
    <tile x="15" y="5" tx="0" ty="0" />
    <tile x="15" y="15" tx="0" ty="0" />
    <tile x="16" y="0" tx="0" ty="0" />
    <tile x="16" y="5" tx="0" ty="0" />
    <tile x="16" y="15" tx="0" ty="0" />
    <tile x="17" y="0" tx="0" ty="0" />
    <tile x="17" y="5" tx="0" ty="0" />
    <tile x="17" y="15" tx="0" ty="0" />
    <tile x="18" y="0" tx="0" ty="0" />
    <tile x="18" y="5" tx="0" ty="0" />
    <tile x="18" y="10" tx="0" ty="0" />
    <tile x="18" y="15" tx="0" ty="0" />
    <tile x="19" y="0" tx="0" ty="0" />
    <tile x="19" y="5" tx="0" ty="0" />
    <tile x="19" y="10" tx="0" ty="0" />
    <tile x="19" y="15" tx="0" ty="0" />
    <tile x="20" y="0" tx="0" ty="0" />
    <tile x="20" y="5" tx="0" ty="0" />
    <tile x="20" y="6" tx="0" ty="0" />
    <tile x="20" y="7" tx="0" ty="0" />
    <tile x="20" y="8" tx="0" ty="0" />
    <tile x="20" y="9" tx="0" ty="0" />
    <tile x="20" y="10" tx="0" ty="0" />
    <tile x="20" y="15" tx="0" ty="0" />
    <tile x="21" y="0" tx="0" ty="0" />
    <tile x="21" y="15" tx="0" ty="0" />
    <tile x="22" y="0" tx="0" ty="0" />
    <tile x="22" y="15" tx="0" ty="0" />
    <tile x="23" y="0" tx="0" ty="0" />
    <tile x="23" y="15" tx="0" ty="0" />
    <tile x="24" y="0" tx="0" ty="0" />
    <tile x="24" y="15" tx="0" ty="0" />
    <tile x="25" y="0" tx="0" ty="0" />
    <tile x="25" y="1" tx="0" ty="0" />
    <tile x="25" y="2" tx="0" ty="0" />
    <tile x="25" y="3" tx="0" ty="0" />
    <tile x="25" y="4" tx="0" ty="0" />
    <tile x="25" y="5" tx="0" ty="0" />
    <tile x="25" y="6" tx="0" ty="0" />
    <tile x="25" y="7" tx="0" ty="0" />
    <tile x="25" y="8" tx="0" ty="0" />
    <tile x="25" y="9" tx="0" ty="0" />
    <tile x="25" y="10" tx="0" ty="0" />
    <tile x="25" y="11" tx="0" ty="0" />
    <tile x="25" y="12" tx="0" ty="0" />
    <tile x="25" y="13" tx="0" ty="0" />
    <tile x="25" y="14" tx="0" ty="0" />
    <tile x="25" y="15" tx="0" ty="0" />
  </Tiles>
</level>